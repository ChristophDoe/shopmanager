package de.awacademy.shopManager.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ShopService {


    private ShopRepository shopRepository;

    @Autowired
    public ShopService(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    public void add(Shop shop){
        shopRepository.save(shop);
    }


    public List<Shop> getShopList() {
        return shopRepository.findAll();
    }

    public Shop getShopById(int shopId) {
        return shopRepository.findById(shopId).orElse(null);
    }


}

