package de.awacademy.shopManager.shop;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class ShopController {

private ShopService shopService;
private ShopRepository shopRepository;

@Autowired
    public ShopController (ShopService shopService, ShopRepository shopRepository) {
    this.shopService = shopService;
    this.shopRepository = shopRepository;
}

@GetMapping ("/shops")
    public String shopIndex (Model model) {
    model.addAttribute("shopList", shopService.getShopList());
    model.addAttribute("shop", new Shop(""));
    return "shop";
}

@PostMapping("/shops")
    public String shopIndex(@ModelAttribute(value = "shop") Shop shop) {
    shopService.add(shop);
    return "redirect:/shops";
}






}
