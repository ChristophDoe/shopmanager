package de.awacademy.shopManager.shop;

import de.awacademy.shopManager.verkäufer.Verkaeufer;

import javax.persistence.*;
import java.util.List;

@Entity
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @OneToMany(mappedBy = "shop")
    private List<Verkaeufer> verkaeuferListe;

    public Shop() {
    }

    public Shop(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Verkaeufer> getVerkaeuferListe() {
        return verkaeuferListe;
    }
}
