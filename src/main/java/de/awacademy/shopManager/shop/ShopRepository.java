package de.awacademy.shopManager.shop;

import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;

public interface ShopRepository extends CrudRepository<Shop, Integer> {

    List<Shop> findAll();
    Optional<Shop> findById(int id);
}
