package de.awacademy.shopManager.verkäufer;


import de.awacademy.shopManager.shop.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VerkaeuferService {

    private VerkaeuferRepository verkaeuferRepository;
    private ShopRepository shopRepository;


    @Autowired
    public VerkaeuferService(VerkaeuferRepository verkaeuferRepository, ShopRepository shopRepository) {
        this.verkaeuferRepository = verkaeuferRepository;
        this.shopRepository = shopRepository;
    }

    public List<Verkaeufer> getVerkaeuferList() {
        return verkaeuferRepository.findAll();
    }


    public Verkaeufer getVerkaeufer(Integer verkaeuferId) {
        Optional<Verkaeufer> verkaeufer = verkaeuferRepository.findById(verkaeuferId);
        return verkaeufer.get();
    }


    public void add(Verkaeufer verkaeuferDTO) {
        verkaeuferRepository.save(verkaeuferDTO);
    }

    public void edit(Verkaeufer verkaeufer, Verkaeufer verkaeuferDTO) {
        verkaeufer.setName(verkaeuferDTO.getName());
        verkaeuferRepository.save(verkaeufer);
    }


    public void delete(Integer verkaeuferId) {
        verkaeuferRepository.deleteById(verkaeuferId);
    }

}
