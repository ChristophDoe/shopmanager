package de.awacademy.shopManager.verkäufer;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VerkaeuferRepository extends CrudRepository<Verkaeufer, Integer> {

    List<Verkaeufer> findAll ();


}
