package de.awacademy.shopManager.verkäufer;

import de.awacademy.shopManager.shop.Shop;

import javax.persistence.*;

@Entity
public class Verkaeufer {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @ManyToOne
    private Shop shop;


    public Verkaeufer () {
    }

    public Verkaeufer(String name, Shop shop) {
        this.name = name;
        this.shop = shop;
    }

    public Integer getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
