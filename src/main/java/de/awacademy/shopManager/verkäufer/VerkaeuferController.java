package de.awacademy.shopManager.verkäufer;


import de.awacademy.shopManager.shop.Shop;
import de.awacademy.shopManager.shop.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class VerkaeuferController {


    private VerkaeuferService verkaeuferService;
    private ShopService shopService;


    @Autowired
    public VerkaeuferController(VerkaeuferService verkaeuferService, ShopService shopService) {
        this.verkaeuferService = verkaeuferService;
        this.shopService = shopService;
    }

    @GetMapping("/{shopId}/verkaeufer/" )
    public String index(Model model, @PathVariable Integer shopId) {
        Shop shop = shopService.getShopById(shopId);
        model.addAttribute("shop", shop);
        model.addAttribute("verkaeufer", new Verkaeufer("", shop));
        return "create";
    }


    @PostMapping("/{shopId}/verkaeufer/add" )
    public String verkaeuferAdd(@ModelAttribute Verkaeufer verkaeufer, @PathVariable Integer shopId) {
        verkaeufer.setShop(shopService.getShopById(shopId));
        verkaeuferService.add(verkaeufer);
        return "redirect:/" + shopId + "/verkaeufer/";
    }


    @GetMapping("/{shopId}/verkaeufer/edit/{verkaeuferId}" )
    public String edit(Model model, @PathVariable Integer verkaeuferId, @PathVariable Integer shopId) {
        model.addAttribute("verkaeufer", verkaeuferService.getVerkaeufer(verkaeuferId));
        model.addAttribute("verkaueferId", verkaeuferId);
        model.addAttribute("shopId", shopId);
        return "edit";
    }

    @PostMapping("/{shopId}/verkaeufer/edit/{verkaeuferId}" )
    public String verkaeuferEditSubmit(@ModelAttribute Verkaeufer verkaeufer, @PathVariable Integer shopId, @PathVariable Integer verkaeuferId) {
        verkaeuferService.edit(verkaeuferService.getVerkaeufer(verkaeuferId), verkaeufer);
        return "redirect:/" + shopId + "/verkaeufer/";
    }

    @PostMapping("/{shopId}/verkaeufer/delete/{verkaeuferId}" )
    public String delete(@PathVariable Integer verkaeuferId, @PathVariable Integer shopId) {
        verkaeuferService.delete(verkaeuferId);
        return "redirect:/" + shopId + "/verkaeufer/";
    }


}
